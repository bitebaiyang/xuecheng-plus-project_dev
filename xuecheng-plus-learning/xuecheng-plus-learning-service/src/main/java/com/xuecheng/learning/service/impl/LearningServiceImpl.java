package com.xuecheng.learning.service.impl;

import com.xuecheng.base.model.RestResponse;
import com.xuecheng.base.utils.StringUtil;
import com.xuecheng.content.model.po.CoursePublish;
import com.xuecheng.learning.feignclient.ContentServiceClient;
import com.xuecheng.learning.feignclient.MediaServiceClient;
import com.xuecheng.learning.model.dto.XcCourseTablesDto;
import com.xuecheng.learning.service.LearningService;
import com.xuecheng.learning.service.MyCourseTableService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class LearningServiceImpl implements LearningService {
    @Autowired
    MyCourseTableService myCourseTableService;

    @Autowired
    ContentServiceClient contentServiceClient;

    @Autowired
    MediaServiceClient mediaServiceClient;
    @Override
    public RestResponse<String> getvideo(String userId, Long courseId, Long teachplanId, String mediaId) {
        //查询课程信息
        CoursePublish coursePublish = contentServiceClient.getCoursepublish(courseId);
        //判断如果为空 不再继续
        if(coursePublish == null){
            return RestResponse.validfail("课程不存在");
        }

        //判断该课程是否可以试学 远程调用内容管理服务
        //也可以从这个对象中解析出课程计划信息去判断是否支持试学
//        String teachplan = coursePublish.getTeachplan();


        if(StringUtil.isNotEmpty(userId)){
            //通过我的课程表 查询学习资格
            XcCourseTablesDto learnstatus = myCourseTableService.getLearnstatus(userId, courseId);
            String learnStatus = learnstatus.getLearnStatus();
            if("702002".equals(learnStatus)){
                return RestResponse.validfail("无法学习，没有选课或者没有支付");
            }else if("702003".equals(learnStatus)){
                return RestResponse.validfail("已过期，重新支付");
            }else{
                //有资格学习 返回视频播放的地址
                //远程调用媒资服务 获取视频播放的地址
                RestResponse<String> playUrlByMediaId = mediaServiceClient.getPlayUrlByMediaId(mediaId);
                return playUrlByMediaId;
            }
        }
        //用户没有登录
        //判断收费规则
        String charge = coursePublish.getCharge();
        if("201000".equals(charge)){
            //有资格学习 返回视频播放地址
            //远程调用媒资获取视频地址
            RestResponse<String> playUrlByMediaId = mediaServiceClient.getPlayUrlByMediaId(mediaId);
            return playUrlByMediaId;
        }
        return RestResponse.validfail("该课程没有选课");
    }
}
