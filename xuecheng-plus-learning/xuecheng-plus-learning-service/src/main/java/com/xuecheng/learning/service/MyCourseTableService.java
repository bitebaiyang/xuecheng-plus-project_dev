package com.xuecheng.learning.service;

import com.xuecheng.base.model.PageResult;
import com.xuecheng.learning.model.dto.MyCourseTableParams;
import com.xuecheng.learning.model.dto.XcChooseCourseDto;
import com.xuecheng.learning.model.dto.XcCourseTablesDto;
import com.xuecheng.learning.model.po.XcCourseTables;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 选课相关的接口
 */
public interface MyCourseTableService {
    public XcChooseCourseDto addChooseCourse(String userId, Long courseId);
    public XcCourseTablesDto getLearnstatus(String userId, Long courseId);
    public boolean saveChooseCourseSuccess(String chooseCourseId);
    public PageResult<XcCourseTables> mycoursetabls(MyCourseTableParams params);
}
