package com.xuecheng.learning.service.impl;

import com.alibaba.fastjson.JSON;
import com.xuecheng.base.exception.XueChengPlusException;
import com.xuecheng.learning.config.PayNotifyConfig;
import com.xuecheng.learning.service.MyCourseTableService;
import com.xuecheng.messagesdk.model.po.MqMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Mr.M
 * @version 1.0
 * @description 接收支付结果
 * @date 2023/2/23 19:04
 */
@Slf4j
@Service
public class ReceivePayNotifyService {
    @Autowired
    MyCourseTableService myCourseTableService;
    @RabbitListener(queues = PayNotifyConfig.PAYNOTIFY_QUEUE)
    public void receive(Message message) throws InterruptedException {
        Thread.sleep(5000);
        //解析出消息
        byte[] body = message.getBody();
        String jsonString = new String(body);

        //转成对象
        MqMessage mqMessage = JSON.parseObject(jsonString, MqMessage.class);
        //解析消息的内容
        //选课 id
        String businessKey1 = mqMessage.getBusinessKey1();
        //选课类型
        String businessKey2 = mqMessage.getBusinessKey2();
        //学习中心服务只要购买课程类的支付订单的结果
        if(businessKey2.equals("60201")){
            //根据消息内容 更新选课记录 向我的课程表插入记录
            boolean b = myCourseTableService.saveChooseCourseSuccess(businessKey1);
            if(!b){
                XueChengPlusException.cast("保存选课记录状态失败");
            }
        }
    }
}

